// ./js/scrtips.js
var local_stor = false; // is local storage available?
const run_key_object = {}
run_key_object.run_key = "";// key for running this
run_key_object.run_key_valid = false;
run_key_object.run_key_type = "proxy"; //  native: key is for omdb; proxy: key is for proxy
run_key_object.run_key_holder = document.getElementById("key-holder-id");//input for run key

const search_title_object = {}
search_title_object.input = document.getElementById("search-title-input-id");
search_title_object.title = "";

var api_request_url_base = "https://wouter.van-den-brink.net/omdb-api-proxy/?key=";
var api_request_url_base_native = "https://www.omdbapi.com/?apikey=";

var result_page = 1;
var pages_loaded = [];
var view_window = document.getElementById("view-window")
var train_holder = document.getElementById("train-holder");// trains of posters
 
var div_diff = 0; // width difference between two divs (div#view-window div#train-holder)
var load_offset_right = 0; // horizontal position of  div#train-holder in div#view-window that signals need to load next page
var load_offset_left = 0;  // horizontal position of  div#train-holder in div#view-window that signals need to load previous page

var back_scroll_right = 0;
var back_scroll_left = 0;

var page_loading = false;// is an api call being performed?
var next_page_load_no = 1;
var prev_page_load_no = 0;
var total_no_movies = 0;// derived from api response
var total_loaded_movies = 0; 
var max_total_loaded_movies = 40;

var old_scroll = 0;// for determining direction of scroll on train? left or right?;

var old_scroll_Y = 0;
var navver = document.getElementById("nav-top");

// var recent_orient_change = false;

/* events and functions for scroll of results */ 
function calcDivDiff(){
    // determine scroll values that trigger api calls for more posters to the right, or restore posters on the left
    div_diff = train_holder.offsetWidth  - view_window.offsetWidth;
    
    load_offset_right = 10 * ( div_diff/10);// trigger to load more posters // - did not realy work
    load_offset_left = 0.3 * ( div_diff/10);// trigger to restore deleted lower number posters

    back_scroll_right = 8 * ( div_diff/10);
    back_scroll_left = 3 * ( div_diff/10);
}
function removeMoviesFromFront(){
    var loaded_movies = train_holder.querySelectorAll(".poster-holder");
    for (var i = 0; i < 10; i++) {
        loaded_movies[i].remove();
    }
    total_loaded_movies -= 10;
    calcDivDiff();
}

function removeMoviesFromEnd(){
    var loaded_movies = train_holder.querySelectorAll(".poster-holder");
    ;
    for (var i = loaded_movies.length -1; i > loaded_movies.length -11; i-- ){
        loaded_movies[i].remove();
    }
    total_loaded_movies -= 10;
    calcDivDiff();
}

////////////////////////////// event listeners
// view_window.addEventListener("mousedown", calcDivDiff);
view_window.addEventListener("pointerdown", calcDivDiff);

/* stop chrome? on android popping up menu when long touching img */
train_holder.addEventListener("contextmenu", function(event){
    event.preventDefault();
    event.stopPropagation
})

train_holder.addEventListener("pointerdown", function (event) {
    event.stopPropagation()
    event.preventDefault();
    /* hide info text to show whole poster */
    if(event.target.tagName == "IMG")
    {
        event.target.nextSibling.nextElementSibling.style.visibility = "hidden"
    }
});

train_holder.addEventListener("pointerout", function(event){
    event.stopPropagation()
    event.preventDefault();
    /* restore visibility info text */
    if(event.target.tagName == "IMG")
    {
        event.target.nextSibling.nextElementSibling.style.visibility = "visible"
    }

});
train_holder.addEventListener("pointerup", function (event) {
    /* restore visibility info text */
    if(event.target.tagName == "IMG")
    {
        event.target.nextSibling.nextElementSibling.style.visibility = "visible"
    }
    if(event.target.classList.contains("button-more")){
        /* fetch the extended info for the movie */
        event.target.nextElementSibling.style.display = "block";
        getViaFetch(
            {
                url: build_search_imdbid(event.target.getAttribute("data-imdbid")),
                type: "movie",
                button_clicked: event.target,
                fetch_type: "movie_info"
            }
        );
    }
});

// remove this? not needed anymore?
search_title_object.input.addEventListener("pointerdown", function(event){
    document.getElementById("error-messages").innerHTML = "";
    document.getElementById("error-messages").style.display = "none";
    search_title_object.input.removeAttribute("disabled")
    //run_key_object.run_key_holder
})

document.getElementById("search_field_button").addEventListener("pointerup",function(){
    search_title_object.input.removeAttribute("disabled");
    search_title_object.input.style.visibility = "visible"
})



window.addEventListener("scroll", function(event){// on body is not working: https://stackoverflow.com/questions/15275969/javascript-scroll-handler-not-firing
    
    //console.log(event.target.body)
    //console.log(event.target.body.scrollTop);
    if(event.target.body.scrollTop > old_scroll_Y){
        old_scroll_Y = event.target.body.scrollTop;
        //console.log("scrolling down")
        if(event.target.body.scrollTop > 20){
            //console.log("greater than 20")
            if(navver.classList.contains("fade-in")){
                //console.log("has fade-in")
                navver.classList.replace("fade-in", "fade-out")
                setTimeout(function() {
                    //your code to be executed after 1 second
                    navver.style.zIndex = "-1000000"
                  }, 500);
                // navver.style.display = "none"
                search_title_object.input.setAttribute("disabled","disabled");
                run_key_object.run_key_holder.setAttribute("disabled","disabled");
            }
            
            //navver.style.position = "fixed";

        }
    }
    if(event.target.body.scrollTop < old_scroll_Y){
        old_scroll_Y = event.target.body.scrollTop;
        //console.log("scrolling up");
        if(event.target.body.scrollTop <= 20){
            
            //navver.style.position = "relative";
        }
        if(navver.classList.contains("fade-out")){
            navver.style.zIndex = "1000000"
            
            navver.classList.replace("fade-out", "fade-in")
            
            

            search_title_object.input.removeAttribute("disabled");
            run_key_object.run_key_holder.removeAttribute("disabled");
        }
    }
});

// !!!!!!!!!!!!!!!!!! put in other listener??? above here?

view_window.addEventListener("scroll", function (event) {
    search_title_object.input.setAttribute("disabled","disabled");
    search_title_object.input.style.visibility = "hidden";

    /* the idea was to start loading and removing posters ahead of reaching the end of the scroll bar:
       this appears not to work to well :-(  */
    var direct_left = false;
    var direct_right = false;
    if(view_window.scrollLeft > old_scroll){
        //console.log("going right");
        direct_right = true;
        direct_left = false;
    }else if(view_window.scrollLeft < old_scroll){
        //console.log("going left");
        direct_left = true
        direct_right = false;
    }
    old_scroll = view_window.scrollLeft;

    var posters = train_holder.querySelectorAll(".poster-holder");
    if(view_window.scrollLeft > load_offset_right && load_offset_right !== 0 && direct_right){
        
        if(!page_loading){
            /* todo: retries? */
            
            next_page_load_no = parseInt(posters[posters.length - 1].getAttribute("data-loadpage")) + 1;
            //console.log(next_page_load_no)

            if(total_loaded_movies < total_no_movies){

                document.getElementById("loading-right").style.display = "block";
                if(total_loaded_movies + 10 > max_total_loaded_movies){
                    removeMoviesFromFront();
                }

                getViaFetch(
                    {
                        url: build_search_name_url(next_page_load_no),
                        type: "right",
                        fetch_type: "movie_list"
                    }
                );
            }
        };
    }
    // if(view_window.scrollLeft < load_offset_left && load_offset_left !== 0 && direct_left){
    // if(view_window.scrollLeft < load_offset_left && direct_left){
    if(view_window.scrollLeft < load_offset_left){
        if(!page_loading ){
            prev_page_load_no = parseInt(posters[0].getAttribute("data-loadpage")) - 1;
            
            if(prev_page_load_no > 0){
                
                document.getElementById("loading-left").style.display = "block";
                
                removeMoviesFromEnd();
                
                getViaFetch(
                    {
                        url: build_search_name_url(prev_page_load_no),
                        type: "left",
                        fetch_type: "movie_list"
                    }
                );
            }
        }
    }

});

/* set key via input */
run_key_object.run_key_holder.addEventListener("change", function (event){

    console.log("change is here: " + event.target.value );
    console.log("local storage is here: " + local_stor );
    
    if(local_stor){
        localStorage.setItem('run_key', event.target.value);
    }
    
    run_key_object.run_key = event.target.value;
    // test the new run_key...
    test_run_key(event.target.value);
});
window.addEventListener("resize", function(event){
    //console.log("resize event:" + recent_orient_change)
    //console.log(event)
    //console.log(event.target)
    if (event.target = "Window"){// part of orientation change
        old_scroll = view_window.scrollLeft;
        //console.log("oldscrol x before:" + old_scroll)
        old_scroll_Y = document.body.scrollTop;
        //console.log(old_scroll_Y)
        //console.log("loffsri before:" + load_offset_right)
        //console.log("scrollwidth:" + view_window.scrollWidth)
        calcDivDiff();
        //console.log("oldscrol x after:" + old_scroll)
        //console.log("loffsri after:" + load_offset_right)
    }
    //console.log(event.target.Window.window)

});
window.addEventListener("orientationchange", function(event) {
    // recent_orient_change = true;
    // a problem here that let it look like the css dom is not fully
    // recalculated. But the real problem is that orientationchange even is 
    // followed by a resize event, and only then the recalculation is completed
    // https://stackoverflow.com/questions/11444838/getting-wrong-screen-height-when-trigger-orientationchange-event/19862606
    //https://stackoverflow.com/questions/21055840/how-can-i-trigger-css-layout-recalculation-after-javascript-dom-updates
    // setTimeout(function(){
    //     recent_orient_change = false;
    // },100);

    // document.body.style.zoom = 1.0000001;
    //document.body.style.scale = "scale(0.5)";
    //setTimeout(function(){
        //document.body.style.zoom = 1;
        //document.body.style.transform = "scale(1)"
        //console.log(old_scroll)
    // old_scroll = view_window.scrollLeft;
    // console.log("oldscrol x before:" + old_scroll)
    // old_scroll_Y = document.body.scrollTop;
    // console.log(old_scroll_Y)
    // console.log("loffsri before:" + load_offset_right)
    // console.log("scrollwidth:" + view_window.scrollWidth)
    // calcDivDiff();
    // console.log("oldscrol x after:" + old_scroll)
    // console.log("loffsri after:" + load_offset_right)
   // },50); //allow some time to flush the css buffer.
    // console.log(old_scroll)
    // old_scroll = view_window.scrollLeft;
    // console.log("oldscrol x before:" + old_scroll)
    // old_scroll_Y = document.body.scrollTop;
    // console.log(old_scroll_Y)
    // console.log("loffsri before:" + load_offset_right)
    // console.log("scrollwidth:" + view_window.scrollWidth)
    // calcDivDiff();
    // console.log("oldscrol x after:" + old_scroll)
    // console.log("loffsri after:" + load_offset_right)

});

///////////////////////////// end of event listeners

/* get key from storage */
// mmmm: https://stackoverflow.com/questions/4201239/in-html5-is-the-localstorage-object-isolated-per-page-domain
if (storageAvailable('localStorage') && localStorage.getItem("run_key") !== null) {
    local_stor = true;
    run_key_object.run_key = localStorage.getItem('run_key');
    run_key_object.run_key_holder.value = run_key_object.run_key;
}

if(local_stor && localStorage.getItem("run_key_valid") !== null ){
    run_key_object.run_key_valid = (localStorage.getItem("run_key_valid") === "true");// produce relevant boolean: (myValue == 'true');// https://stackoverflow.com/questions/263965/how-can-i-convert-a-string-to-boolean-in-javascript
    //console.log(typeof(run_key_object.run_key_valid));
}
if(local_stor && localStorage.getItem("run_key_type") !== null ){
    run_key_object.run_key_type = localStorage.getItem("run_key_type")
}

function test_run_key(r_key, iter = 0){
    console.log("test run key activated: " + r_key + " " + iter);

    if(iter < 2){
        console.log(" test run_key on proxy");
        getTestViaFetch(
            {
                url: build_authorization_test_url(),
                    fetch_type: "auth_test",
                    type: run_key_object.run_key_type,
                    iter: iter + 1,
                    r_key: r_key

            }
        );

    }else{
        document.getElementById("error-messages").innerHTML = "";
        document.getElementById("error-messages").style.display = "block";
        var error_para = document.createElement("p");
        error_para.innerText = 'De ingevoerde sleutel wordt niet aanvaard, sorry';
        console.log(error_para.innerText);          
        document.getElementById("error-messages").append(error_para);

    }

};
function test_run_keyL(r_key, iter = 0){
    if(run_key_object.run_key_type == "proxy" && iter < 2){//typeof variable !== 'undefined'
            console.log("test run_key on proxy");
            // getViaFetch(
            //     {
            //         url: build_authorization_test_url(),
            //         fetch_type: "auth_test",
            //         type: run_key_object.run_key_type,
            //         iter: iter + 1,
            //         r_key: r_key
            //     }
            // );

    }else
    if(run_key_object.run_key_type == "native" && iter < 2){
        console.log("test run_key on omdb");
        // getViaFetch(
        //     {
        //         url: build_authorization_test_url(),
        //         type: run_key_object.run_key_type,
        //         fetch_type: "auth_test",
        //         iter: iter + 1,
        //         r_key: r_key
        //     }
        // );

    }else {

        console.log("unkown run_key type");
        console.log("unkown run_key type dus");
        document.getElementById("error-messages").innerHTML = "";
        document.getElementById("error-messages").style.display = "block";
        var error_para = document.createElement("p");
        error_para.innerText = 'De ingevoerde sleutel wordt niet aanvaard, sorry';
        console.log(error_para.innerText);
                //response.status;
                        
        document.getElementById("error-messages").append(error_para);

                //page_loading = false
               // return;

    }
}


//localStorage.setItem('run_key_valid', false);// debug
if(!run_key_object.run_key_valid){
    console.log("run key is not valid");
    test_run_key(run_key_object.run_key);
}

function build_authorization_test_url(){
    if(run_key_object.run_key_type == "proxy"){
        //console.log(api_request_url_base);
        return api_request_url_base + run_key_object.run_key;
    }else if(run_key_object.run_key_type == "native"){
        //console.log(api_request_url_base_native);
        return api_request_url_base_native + run_key_object.run_key;
    }else{
        //console.log("error: run_key is invallid");
        document.getElementById("error-messages").innerHTML = "";
        document.getElementById("error-messages").style.display = "block";
        var error_para = document.createElement("p");
        error_para.innerText = "Uw sleutel is niet geldig helaas";
        var error_para2 = document.createElement("p");
        error_para2.innerText = "Kijkt u deze goed na?"
        document.getElementById("error-messages").append(error_para2);
        document.getElementById("error-messages").append(error_para);
        //todo: show error message
    }
    
}
function build_search_imdbid(imdbid){
    if(run_key_object.run_key_type === "proxy"){
        return api_request_url_base + run_key_object.run_key + "&i=" + imdbid + "&plot=full";
    }else if(run_key_object.run_key_type === "native"){
        return api_request_url_base_native + run_key_object.run_key + "&i=" + imdbid + "&plot=full";
    }
    
}
function build_search_name_url(page_no){
    if(run_key_object.run_key_type === "proxy"){
        return api_request_url_base + run_key_object.run_key + "&s=" + search_title_object.title + "&type=movie" + "&page=" + page_no;
    }else if(run_key_object.run_key_type === "native"){
        return api_request_url_base_native + run_key_object.run_key + "&s=" + search_title_object.title + "&type=movie" + "&page=" + page_no;
    }
}

search_title_object.input.addEventListener("change", function(event) {
    //console.log("change search_title_object.input")
    //remove the posters from previous query
    var posters = document.getElementById("train-holder").querySelectorAll(".poster-holder");
    posters.forEach(function(poster){
        poster.remove();
    });
    total_loaded_movies = 0;
    total_no_movies = 0;
    next_page_load_no = 1;
    prev_page_load_no = 0;
    old_scroll = 0;
    
    search_title_object.title = encodeURI(event.target.value);
    //console.log(search_title_object.title);
    //console.log(build_search_name_url(next_page_load_no))
    
    getViaFetch(
        {
            url: build_search_name_url(next_page_load_no),
            type: "right",
            fetch_type: "movie_list"
        }
    );
    this.blur();// remove focus
});

document.getElementById("modal-holder").addEventListener("pointerup", function(event){
    if(event.target.classList.contains("closeX")){
        document.getElementById("blocker").style.display = "none"
        document.body.style.overflow = "auto"
        document.getElementById("movie-info-modal").style.display = "none";
        //reblur
        search_title_object.input.blur();
    }
})

var containter = document.querySelector("#train-holder");
var templ = document.querySelector("#search-result-tmpl");

function getTestViaFetch(get_test_url_object){
    console.log(get_test_url_object)
    var get_url = get_test_url_object.url;
    if(page_loading === true){
        // == versus ===  : https://www.jstips.co/en/javascript/use_===_instead_of_==/ :: if would be faster?
        //// page_loading : if it is true: pause before retry, after 4 retries, error
        console.log("still loading, trying again in .. seconds");
        // todo: retries
    }else{
        page_loading = true;

        fetch(get_url)
        .then(function(response){
            console.log(response);
            if(response.status === 401){
                console.log("status 401, unauthorized " + response.status);
                console.log(response.json);
                if(local_stor){
                    localStorage.setItem('run_key_valid', false);
                    localStorage.setItem("run_key_type", run_key_object.run_key_type);
                }
                run_key_object.run_key_valid = false;
                response.json().then(function(data){
                    console.log(data);
                    page_loading = false;
                    if(get_test_url_object.type == "proxy"){
                        run_key_object.run_key_type = "native";
                    }
                    if(get_test_url_object.type == "native"){
                        run_key_object.run_key_type = "proxy";
                    }
                    test_run_key(get_test_url_object.r_key,get_test_url_object.iter);
                });
                // try the other?
            }else if(response.status === 200){
                response.json().then(function(data){
                    console.log("200 case");
                    console.log(data);
                    if(local_stor){
                        localStorage.setItem('run_key_valid', true);
                        localStorage.setItem("run_key_type", run_key_object.run_key_type);
                    }
                    run_key_object.run_key_valid = true;
                    
                    page_loading = false;
                    document.getElementById("error-messages").innerHTML = `<h2)>Welkom bij Filmzoekert!</h2>
                    <h3>Een toelichting:</h3>
                    <ul>
                        <li>Heb een sleutel</li>
                        <li>Voer sleutel in</li>
                        <li>Voer filmzoekterm in</li>
                        <li>Scroll door resultaten</li>
                        <li>(muis neer houden/blijven aanraken plaatje voor geheel tonen ervan)</li>
                        <li>klik/tap(tik) knop "meer info" voor meer informatie</li>
                        
                    </ul>`
                });


            }else{
                if(local_stor){
                    localStorage.setItem('run_key_valid', false);
                    localStorage.setItem("run_key_type", run_key_object.run_key_type);
                }
                run_key_object.run_key_valid = false;
                
                //console.log(response.status)
                //console.log(response)
                response.json().then(function(data){
                    //console.log("? case?". response.status)
                    //console.log(data)
                    page_loading = false;

                });
            }
            
        })
        .catch(function(err) {
            console.log('Test Fetch Error :-S', err);
            page_loading = false;
        });
    }

}

function getViaFetch(get_url_object){
    //console.log(get_url_object)
    var get_url = get_url_object.url;
    var url_type = get_url_object.type;
    
    if(page_loading === true){
        // == versus ===  : https://www.jstips.co/en/javascript/use_===_instead_of_==/ :: if would be faster?
        //// page_loading : if it is true: pause before retry, after 4 retries, error
        console.log("still loading, trying again in .. seconds");
        // todo: retries
    }else{
        page_loading = true;
        
    fetch(get_url)
    .then(
        function(response) {
            if(response.status === 401){
                //console.log("http code: " + response.status)
                response.json().then(function(data) {
                    console.log(data);
                });
                if(get_url_object.type == "proxy"){
                    run_key_object.run_key_type = "native";
                }else if(get_url_object.type == "native"){
                    run_key_object.run_key_type = "proxy";
                }
                page_loading = false;
                test_run_key(get_url_object.r_key,get_url_object.iter);
                
            }else 
            if (response.status !== 200 && response.status !== 401) {
                console.log('Looks like there was a problem. Status Code: ' +
                response.status);

                document.getElementById("error-messages").innerHTML = "";
                document.getElementById("error-messages").style.display = "block";
                var error_para = document.createElement("p");
                error_para.innerText = 'Looks like there was a problem. Status Code: ' +
                response.status;
                        
                document.getElementById("error-messages").append(error_para);

                page_loading = false
                return;
            }else{
                // Examine the text in the response
            response.json().then(function(data) {
                    
                if(data.Response === "True"){
                    if(url_type == "movie"){
                
                            var movie_info_modal = document.getElementById("movie-info-modal");
                            var movie_info_pre = movie_info_modal.querySelector("pre");
                            if (movie_info_pre){
                                movie_info_pre.remove();
                            }
                            var json_string = JSON.stringify(data, null, 2); // spacing level = 2 // get pretty json
                            var pre_tag = document.createElement("pre");
                            pre_tag.innerText = json_string
                            movie_info_modal.appendChild(pre_tag);
                            
                            document.getElementById("blocker").style.display = "block"
                            document.body.style.overflow = "hidden"
                            document.getElementById("movie-info-modal").style.display = "block";
                            get_url_object.button_clicked.nextElementSibling.style.display = "none";

                            page_loading = false;
                        }else{
                            // console.log(data.totalResults);
                            total_no_movies = parseInt(data.totalResults);
                            total_loaded_movies += data.Search.length; 

                            function forAction(url_type){
                               
                                var clone = templ.content.firstElementChild.cloneNode(true);
                                var textpanel = clone.querySelectorAll(".poster-info")[0];
                                textpanel.setAttribute("readonly", "readonly");
    
                                var button_more = document.createElement("button");
                                button_more.classList.add("button-more");
                                button_more.setAttribute("type", "button");
                                button_more.setAttribute("data-imdbID", data.Search[i].imdbID);
                                var button_text = document.createTextNode("Meer informatie");
                                button_more.appendChild(button_text);
                                textpanel.appendChild(button_more);

                                var loading_info = document.createElement("div");
                                loading_info.classList.add("loading-more-info");
                                loading_info.style.display = "none";
                                loading_info.innerHTML = "<p>loading more</p>";
                                textpanel.appendChild(loading_info);
    
                                var para_title = document.createElement("p");
                                para_title.classList.add("poster-title");
                                var title = document.createTextNode("Titel: " + data.Search[i].Title);
                                para_title.appendChild(title); 
                                textpanel.appendChild(para_title);
    
                                var para_year = document.createElement("p");
                                para_year.classList.add("poster-year");
                                var year = document.createTextNode("Jaar: " + data.Search[i].Year);
                                para_year.appendChild(year); 
                                textpanel.appendChild(para_year);
    
                                var para_imdbID = document.createElement("p");
                                para_imdbID.classList.add("poster-imdbID");
                                var imdbID = document.createTextNode("imdbID: " + data.Search[i].imdbID);
                                para_imdbID.appendChild(imdbID); 
                                textpanel.appendChild(para_imdbID);

                                var imgdus = clone.querySelectorAll("img")[0];
                                imgdus.setAttribute("readonly", "readonly");
                                imgdus.classList.add("poster-img");
                                if(url_type == "right"){
                                    clone.setAttribute("data-loadpage", next_page_load_no);
                                }
                                if(url_type == "left"){
                                    clone.setAttribute("data-loadpage", prev_page_load_no);
                                }
                                
                                if(data.Search[i].Poster !== "N/A"){
                                    imgdus.setAttribute("src", data.Search[i].Poster);
                                    imgdus.setAttribute("alt", data.Search[i].Title + "," + data.Search[i].imdbID  );
                                    imgdus.setAttribute("data-info", JSON.stringify(data.Search));
                                    //containter.appendChild(clone); //!!!!!!!!!!!!! what if; for prepend or something
                                    //if(get_url_object.side === "right"){
                                    if(url_type == "right"){
                                        containter.append(clone); //!!!!!!!!!!!!! what if; for prepend or something
                                    }else
                                    if(url_type == "left"){
                                        containter.prepend(clone); //!!!!!!!!!!!!! what if; for prepend or something
                                    }
                                    
        
                                }else{
                                    imgdus.remove();
                                    //poster_holder = clone.querySelectorAll(".poster-holder")[0];
                                    var texthere = document.createTextNode("geen poster hier");
    
                                    clone.appendChild(texthere);
                                
                                    if(url_type == "right"){
                                        containter.append(clone); //!!!!!!!!!!!!! what if; for prepend or something
                                    }else
                                    if(url_type == "left"){
                                        containter.prepend(clone); //!!!!!!!!!!!!! what if; for prepend or something
                                    }
                                }
                                
                                page_loading = false;
                                if(url_type == "right"){
                                    
                                    document.getElementById("loading-right").style.display = "none"
                                    //view_window.scrollBy(back_scroll_right,0);
                                    //view_window.scrollLeft = back_scroll_right;
                                }
                                if(url_type == "left"){
                                
                                    document.getElementById("loading-left").style.display = "none"
                                    //view_window.scrollBy(back_scroll_left,0);
                                    view_window.scrollLeft = back_scroll_left;
                                }
                                
                            }

                        if(url_type == "right"){
                            for (var i = 0; i < data.Search.length; i++) {
                                forAction(url_type);
                            }
                        }else if(url_type == "left"){
                            for (var i = data.Search.length-1; i >= 0;i--){
                                forAction(url_type);

                            }
                        }
                        
                        calcDivDiff();
                        }

                    }else{

                        console.log(data);
                        console.log(get_url_object);
                        console.log(get_url_object.type);
                        console.log(data.Error);
                        // if(data.hasOwnProperty("error")){
                        //     console.log("geteste error" + data.error);
                        //     console.log(get_url_object.fetch_type);
                        //     console.log(get_url_object.type);
                        //     console.log(get_url_object.iter);
                        //     console.log(get_url_object.r_key);
                        //     if(get_url_object.type == "proxy"){
                        //         run_key_object.run_key_type = "native";
                        //     }else if(get_url_object.type == "native"){
                        //         run_key_object.run_key_type = "proxy";
                        //     }
                        //     page_loading = false;
                        //     test_run_key(get_url_object.r_key,get_url_object.iter);
                        //     return;
                        // }
                        if(data.hasOwnProperty("Error") && get_url_object.fetch_type == "auth_test"){
                            console.log("geteste Error" + data.Error);
                            
                            console.log(get_url_object.fetch_type);
                            console.log(get_url_object.type);
                            console.log(get_url_object.iter);
                            console.log(get_url_object.r_key);
                            // if(get_url_object.type == "proxy"){
                            //     run_key_object.run_key_type = "native";
                            // }else if(get_url_object.type == "native"){
                            //     run_key_object.run_key_type = "proxy";
                            // }
                            // page_loading = false;
                            // test_run_key(get_url_object.r_key,get_url_object.iter);

                        }else if(data.hasOwnProperty("Error") && get_url_object.fetch_type == "movie_list"){
                            // this part needs an if: movie not found
                        document.getElementById("error-messages").innerHTML = "";
                        document.getElementById("error-messages").style.display = "block";
                        var error_para = document.createElement("p");
                        error_para.innerText = "Geen films gevonden, sorry";
                        var error_para2 = document.createElement("p");
                        error_para2.innerText = "Misschien een andere zoekterm?"
                        document.getElementById("error-messages").append(error_para2);
                        document.getElementById("error-messages").append(error_para);

                        }
                        

                        page_loading = false;

                    }
                });

            }

            
            }
        )
        .catch(function(err) {
            console.log('Fetch Error :-S', err);
        });
    }
}

//////////////////////////////////////////////////////////////
function storageAvailable(type) {
    // https://developer.mozilla.org/en-US/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
    var storage;
    try {
        storage = window[type];
        var x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length !== 0);
    }
}